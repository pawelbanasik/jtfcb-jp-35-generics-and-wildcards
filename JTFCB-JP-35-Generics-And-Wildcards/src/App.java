import java.util.ArrayList;

// wildcards to jest juz zaawansowany temat

class Machine {

	@Override
	public String toString() {
		return "I am a Machine";
	}
	
	// dodal ta metode zeby pokazac uzycie wildcards
	// tzw upperband
	// do przykladu 3
	public void start() {
		
		System.out.println("Machine starting.");
		
	}

}

class Camera extends Machine {

	@Override
	public String toString() {
		return "I am a camera";
	}
	
	public void snap () {
		
		System.out.println("Snap");
		
	}

}

public class App {

	public static void main(String[] args) {

		// Przyklad 1
		// ArrayList<String> list = new ArrayList<String>();
		// list.add("one");
		// list.add("two");
		// showList(list);

		// Przyklad 2
		ArrayList<Machine> list1 = new ArrayList<Machine>();
		list1.add(new Machine());
		list1.add(new Machine());

		ArrayList<Camera> list2 = new ArrayList<Camera>();
		list2.add(new Camera());
		list2.add(new Camera());
		showList(list2);

	}

	// Przyklad 1
	// dlatego static bo nie mamy obiektu klassy app
	// zeby przywolac ta metode nie majac obiektu app musi byc static
	// jako argument daje ArrayList
	// jesli masz Arraylist ze stringami i chesz podac do metody
	// wtedy trzeba poamietac ze typ zawiera parametr z typem w takich nawiasach
	// <>
	// public static void showList(ArrayList<String> list) {
	// for (String value : list) {
	// System.out.println(value);

	// }

	// Przyklad 2
// ciezko przekazac arraylist of acmeras do array list of machines
	// dajemy ten znak zapytania i to jest tzw wildcard!!!
	//public static void showList(ArrayList<?> list) {
		// dodstkwo zmieniasz ta linijke z Machine na Object
		// for (Machine value : list) {
		//for (Object value : list) {
		//	System.out.println(value);

	//Przyklad 3
			public static void showList(ArrayList<? extends Machine> list) {
				// juz linijka object nie jest potrzebna tylko moze byc Machine
				for (Machine value : list) {
					System.out.println(value);
			value.start();
			
			
		}
	}

}
